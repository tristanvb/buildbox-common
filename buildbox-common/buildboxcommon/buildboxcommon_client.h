/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCOMMON_CLIENT
#define INCLUDED_BUILDBOXCOMMON_CLIENT

#include <functional>
#include <memory>
#include <unordered_map>

#include <buildboxcommon_casclient.h>
#include <buildboxcommon_cashash.h>
#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_grpcclient.h>
#include <buildboxcommon_grpcerror.h>
#include <buildboxcommon_grpcretrier.h>
#include <buildboxcommon_merklize.h>
#include <buildboxcommon_protos.h>
#include <buildboxcommon_requestmetadata.h>

namespace buildboxcommon {

/**
 * Implements a mechanism to communicate with remote CAS servers, and includes
 * data members to keep track of an ongoing batch upload or batch download
 * request.
 */
class Client : public CASClient {
  public:
    Client(DigestFunction_Value digest_function =
               DigestFunction_Value::DigestFunction_Value_SHA256)
        : CASClient(std::make_shared<GrpcClient>(), digest_function)
    {
    }

    /**
     * Connect to the CAS server with the given connection options.
     */
    void init(const ConnectionOptions &options, bool checkCapabilities = true);

    /**
     * Connect to the CAS server with the given clients.
     */
    void
    init(std::shared_ptr<ByteStream::StubInterface> bytestreamClient,
         std::shared_ptr<ContentAddressableStorage::StubInterface> casClient,
         std::shared_ptr<LocalContentAddressableStorage::StubInterface>
             d_localCasClient,
         std::shared_ptr<Capabilities::StubInterface> capabilitiesClient,
         size_t maxBatchTotalSizeBytes = 0);

    void set_tool_details(const std::string &tool_name,
                          const std::string &tool_version);
    /**
     * Set the optional ID values to be attached to requests.
     */
    void set_request_metadata(const std::string &action_id,
                              const std::string &tool_invocation_id,
                              const std::string &correlated_invocations_id);

    std::string instanceName() const;

    void setInstanceName(const std::string &instance_name);

    int retryLimit() const;

    void setRetryLimit(int limit);

  protected:
    void issueRequestUpdateStatsAndThrowOnErrors(
        const buildboxcommon::GrpcRetrier::GrpcInvocation &invocation,
        const std::string &invocationName,
        GrpcClient::RequestStats *requestStats) const;

  private:
    /*
     * RequestMetadata values. They will be attached to requests sent by this
     * client.
     */
    GrpcRetrier makeRetrier(const GrpcRetrier::GrpcInvocation &invocation,
                            const std::string &name) const;
};

} // namespace buildboxcommon

#endif
