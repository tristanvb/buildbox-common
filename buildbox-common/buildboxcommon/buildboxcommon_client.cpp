﻿/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_client.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_mergeutil.h>

#include <algorithm>
#include <chrono>
#include <errno.h>
#include <fcntl.h>
#include <fstream>
#include <grpc/grpc.h>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <unistd.h>
#include <uuid/uuid.h>

namespace buildboxcommon {

void Client::init(const ConnectionOptions &options, bool checkCapabilities)
{
    d_grpcClient = std::make_shared<GrpcClient>();
    d_grpcClient->init(options);
    CASClient::init(checkCapabilities);
}

void Client::init(
    std::shared_ptr<ByteStream::StubInterface> bytestreamClient,
    std::shared_ptr<ContentAddressableStorage::StubInterface> casClient,
    std::shared_ptr<LocalContentAddressableStorage::StubInterface>
        localCasClient,
    std::shared_ptr<Capabilities::StubInterface> capabilitiesClient,
    size_t maxBatchTotalSizeBytes)
{
    CASClient::init(bytestreamClient, casClient, localCasClient,
                    capabilitiesClient, maxBatchTotalSizeBytes);
}

std::string Client::instanceName() const
{
    return d_grpcClient->instanceName();
}

void Client::setInstanceName(const std::string &instance_name)
{
    d_grpcClient->setInstanceName(instance_name);
}

void Client::set_tool_details(const std::string &tool_name,
                              const std::string &tool_version)
{
    d_grpcClient->setToolDetails(tool_name, tool_version);
}

void Client::set_request_metadata(const std::string &action_id,
                                  const std::string &tool_invocation_id,
                                  const std::string &correlated_invocations_id)
{
    d_grpcClient->setRequestMetadata(action_id, tool_invocation_id,
                                     correlated_invocations_id);
}

void Client::issueRequestUpdateStatsAndThrowOnErrors(
    const GrpcRetrier::GrpcInvocation &invocation,
    const std::string &invocationName,
    GrpcClient::RequestStats *requestStats) const
{
    d_grpcClient->issueRequest(invocation, invocationName, requestStats);
}

GrpcRetrier Client::makeRetrier(const GrpcRetrier::GrpcInvocation &invocation,
                                const std::string &invocationName) const
{
    return d_grpcClient->makeRetrier(invocation, invocationName);
}

int Client::retryLimit() const { return d_grpcClient->retryLimit(); }

void Client::setRetryLimit(int limit) { d_grpcClient->setRetryLimit(limit); }

} // namespace buildboxcommon
